﻿using Notifications.Service.Application.EmailNotifications.Responses;
using Notifications.Service.Domain.Email;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notifications.Service.Application.EmailNotifications.Extensions
{
    public static class EmailNotificationExtension
    {
        public static NotificationDetailsResponse ToDetailsResponse(this EmailNotification emailNotification)
        {
            if (emailNotification == null)
                throw new ArgumentException("Invalid operation, cannot convert null to details response");

            return new NotificationDetailsResponse
            {
                NotificationContent = emailNotification?.EmailContent,
                NotificationStatus = emailNotification.Operation.OperationStatusId,
                Recipients = emailNotification?.Recipients?.Select(x => x.EmailAddress),
                Sender = emailNotification?.Sender
            };
        }

        public static AllNotificationResponse ToAllNotificationsResponse(this IEnumerable<EmailNotification> emailNotifications)
        {
            if (emailNotifications == null)
                throw new ArgumentException("Invalid operation, cannot convert null to all notifications response");

            var result = new AllNotificationResponse();

            result.NotificationResponses = emailNotifications.Select(notification => new NotificationResponse
            {
                NotificationContent = notification.EmailContent,
                Recipients = notification?.Recipients?.Select(x => x.EmailAddress),
                Sender = notification.Sender
            });

            return result;
        }

        public static bool NotificationHasBeenSend(this EmailNotification emailNotifications)
        {
            if (emailNotifications == null)
                throw new ArgumentException("Invalid operation");

            return emailNotifications.Operation.OperationStatusId == Domain.Operation.OperationStatusEnum.Send;
        }
    }
}
