# README #

1. Application use Sqlserver as database (not in memory Db)

	Update connection string in proper appsettings

	"ConnectionStrings": {
		"DefaultConnection": ""
	  },
  
2. Run migrations on Notification.Service.Repositories -> update-database

3. Setup Notification.Service.Api as startup project and run it.

4. Application run on port :10000