﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using NLog.Web;
using Notifications.Service.Api.Models;
using System;
using System.Net;

namespace Notifications.Service.Api.Extensions
{
    public static class ExceptionMiddlewareExtension
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseExceptionHandler(error =>
            {
                error.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        string message =  "";
                        var aggregate = contextFeature.Error as AggregateException;

                        if (aggregate != null)
                        {
                            message = string.Join(",", aggregate.InnerExceptions);
                        }
                        else
                        {
                            message = contextFeature.Error.Message;
                        }

                        var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

                        logger.Error(contextFeature.Error);

                        await context.Response.WriteAsync(
                            new ErrorDetails
                            {
                                StatusCode = context.Response.StatusCode,
                                Message = message
                            }.ToString());
                    }
                });
            });
        }
    }
}
