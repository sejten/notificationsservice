﻿namespace Notifications.Service.Domain.Constants
{
    public static class EmailNotifiations
    {
        public const string InitializeNotificationStatus = "email notification has been initialized";
        public const string UpdatedNotificationRecipients = "email notification recipients updated";
        public const string RemovedNotificationRecipients = "email notification recipients removed";
        public const string UpdatedNotificationSender = "email notification sender updated";
        public const string NotificationHasBeenSended = "email notification has been sended";

    }
}
