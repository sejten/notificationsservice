﻿namespace Notifications.Service.Api.Models
{
    public class EmailRecipientDto
    {
        public string Email { get; set; }
    }
}
