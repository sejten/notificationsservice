﻿using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Exceptions;
using Notifications.Service.Application.EmailNotifications.Extensions;
using Notifications.Service.Repositories.Repositories.Email;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Text;

namespace Notifications.Service.Application.EmailNotifications.Handlers
{
    public sealed class UpdateEmailNotificationRecipientCommandHandler 
        : IRequestHandler<UpdateEmailNotificationRecipientCommand>
    {
        private readonly IEmailNotificationRepository _emailMetadataRepository;

        public UpdateEmailNotificationRecipientCommandHandler(
            IEmailNotificationRepository emailMetadataRepository)
        {
            this._emailMetadataRepository = emailMetadataRepository;
        }

        public async Task<Unit> Handle(UpdateEmailNotificationRecipientCommand request, CancellationToken cancellationToken)
        {
            ValidateRequestRecipients(request.Recipients);

            var notification = await _emailMetadataRepository.GetByIdAsync(request.NotificationId);

            if (notification == null)
                throw new BusinessLogicException($"Notification with given Id {request.NotificationId} not found");

            if (notification.NotificationHasBeenSend())
                throw new BusinessLogicException($"Notification with given Id {request.NotificationId} is is already send");

            var unusedMailsToRemove = notification.GetItemsToRemove(request.Recipients);

            if (unusedMailsToRemove.Any())
                notification.RemoveRecipients(unusedMailsToRemove);

            notification.UpdateRecipients(request.Recipients);

            await _emailMetadataRepository.UpdateNotificationAsync(notification, unusedMailsToRemove);

            return Unit.Value;
        }

        private void ValidateRequestRecipients(IEnumerable<string> recipients)
        {
            if (recipients == null 
                || !recipients.Any())
            {
                throw new BusinessLogicException($"Cannot update recipients with empty list of recepients");
            }

            StringBuilder stringBuilder = new StringBuilder();

            foreach (var recipient in recipients)
            {
                if (!recipient.IsValidEmailAddress())
                    stringBuilder.Append($"Given recipient email {recipient} is not valid email. ");
            }

            if (stringBuilder.Length > 0)
            {
                throw new BusinessLogicException(stringBuilder.ToString());
            }
        }
    }
}
