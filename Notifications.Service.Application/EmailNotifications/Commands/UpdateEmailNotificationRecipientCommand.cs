﻿using MediatR;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Commands
{
    public sealed class UpdateEmailNotificationRecipientCommand : IRequest
    {
        public long NotificationId { get; }
        public IEnumerable<string> Recipients { get; }
        public UpdateEmailNotificationRecipientCommand(
            long notificationId, 
            IEnumerable<string> recipients)
        {
            NotificationId = notificationId;
            Recipients = recipients;
        }
    }
}
