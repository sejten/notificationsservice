﻿using Notifications.Service.Domain.Email;
using System;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Validators
{
    public interface IEmailNotificationValidator
    {
        public bool CanValidate(EmailNotification emailNotification);
        public IEnumerable<Exception> Validate(EmailNotification emailNotification);
    }
}
