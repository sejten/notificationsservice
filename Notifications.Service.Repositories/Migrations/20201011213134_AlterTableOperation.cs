﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AlterTableOperation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailNotification_OperationMetadata_OperationId",
                table: "EmailNotification");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationHistory_OperationMetadata_OperationMetadataId",
                table: "OperationHistory");

            migrationBuilder.DropTable(
                name: "OperationMetadata");

            migrationBuilder.DropIndex(
                name: "IX_OperationHistory_OperationMetadataId",
                table: "OperationHistory");

            migrationBuilder.DropColumn(
                name: "OperationMetadataId",
                table: "OperationHistory");

            migrationBuilder.AddColumn<long>(
                name: "OperationId",
                table: "OperationHistory",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Operation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModificationDate = table.Column<DateTime>(nullable: false),
                    OperationStatusId = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operation", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OperationHistory_OperationId",
                table: "OperationHistory",
                column: "OperationId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailNotification_Operation_OperationId",
                table: "EmailNotification",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationHistory_Operation_OperationId",
                table: "OperationHistory",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailNotification_Operation_OperationId",
                table: "EmailNotification");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationHistory_Operation_OperationId",
                table: "OperationHistory");

            migrationBuilder.DropTable(
                name: "Operation");

            migrationBuilder.DropIndex(
                name: "IX_OperationHistory_OperationId",
                table: "OperationHistory");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "OperationHistory");

            migrationBuilder.AddColumn<long>(
                name: "OperationMetadataId",
                table: "OperationHistory",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OperationMetadata",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModificationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OperationStatusId = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationMetadata", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OperationHistory_OperationMetadataId",
                table: "OperationHistory",
                column: "OperationMetadataId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailNotification_OperationMetadata_OperationId",
                table: "EmailNotification",
                column: "OperationId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationHistory_OperationMetadata_OperationMetadataId",
                table: "OperationHistory",
                column: "OperationMetadataId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
