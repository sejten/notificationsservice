﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Notifications.Service.Repositories;

namespace Notifications.Service.Repositories.Migrations
{
    [DbContext(typeof(NotificationsDbContext))]
    [Migration("20201010123610_AlterColumn_OperationStatus_Name")]
    partial class AlterColumn_OperationStatus_Name
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Notifications.Service.Domain.Email.EmailMetadata", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("EmailContent")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ModificationDate")
                        .HasColumnType("datetime2");

                    b.Property<long?>("OperationId")
                        .HasColumnType("bigint");

                    b.Property<int?>("SenderId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("OperationId");

                    b.HasIndex("SenderId");

                    b.ToTable("EmailMetadata");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Email.EmailRecipient", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("EmailMetadataId")
                        .HasColumnType("bigint");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("EmailMetadataId");

                    b.ToTable("EmailRecipient");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Email.EmailSender", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("SenderEmail")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SenderName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("EmailSender");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Operation.OperationHistory", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("OperationInfo")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("OperationMetadataId")
                        .HasColumnType("bigint");

                    b.Property<string>("OperationPayload")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OperationType")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("OperationMetadataId");

                    b.ToTable("OperationHistorie");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Operation.OperationMetadata", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ModificationDate")
                        .HasColumnType("datetime2");

                    b.Property<byte?>("OperationStatusId")
                        .HasColumnType("tinyint");

                    b.Property<byte>("StatusId")
                        .HasColumnType("tinyint");

                    b.HasKey("Id");

                    b.HasIndex("OperationStatusId");

                    b.ToTable("OperationMetadata");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Operation.OperationStatus", b =>
                {
                    b.Property<byte>("Id")
                        .HasColumnType("tinyint");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("OperationStatus");

                    b.HasData(
                        new
                        {
                            Id = (byte)0,
                            Name = "Pending"
                        },
                        new
                        {
                            Id = (byte)1,
                            Name = "Send"
                        },
                        new
                        {
                            Id = (byte)2,
                            Name = "Failed"
                        });
                });

            modelBuilder.Entity("Notifications.Service.Domain.Email.EmailMetadata", b =>
                {
                    b.HasOne("Notifications.Service.Domain.Operation.OperationMetadata", "Operation")
                        .WithMany()
                        .HasForeignKey("OperationId");

                    b.HasOne("Notifications.Service.Domain.Email.EmailSender", "Sender")
                        .WithMany()
                        .HasForeignKey("SenderId");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Email.EmailRecipient", b =>
                {
                    b.HasOne("Notifications.Service.Domain.Email.EmailMetadata", null)
                        .WithMany("Recipients")
                        .HasForeignKey("EmailMetadataId");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Operation.OperationHistory", b =>
                {
                    b.HasOne("Notifications.Service.Domain.Operation.OperationMetadata", null)
                        .WithMany("OperationInfos")
                        .HasForeignKey("OperationMetadataId");
                });

            modelBuilder.Entity("Notifications.Service.Domain.Operation.OperationMetadata", b =>
                {
                    b.HasOne("Notifications.Service.Domain.Operation.OperationStatus", "OperationStatus")
                        .WithMany()
                        .HasForeignKey("OperationStatusId");
                });
#pragma warning restore 612, 618
        }
    }
}
