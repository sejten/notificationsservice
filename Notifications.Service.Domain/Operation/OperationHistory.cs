﻿using Notifications.Service.Domain.Abstract;
using System.ComponentModel.DataAnnotations;

namespace Notifications.Service.Domain.Operation
{
    public class OperationHistory : ImmutableEntity<long>
    {
        [Required]
        public string OperationType { get; private set; }

        [Required]
        public string OperationInfo { get; private set; }

        [Required]
        public string OperationPayload { get; private set; }

        public OperationHistory(string operationType, string operationInfo, string operationPayload)
        {
            OperationType = operationType;
            OperationInfo = operationInfo;
            OperationPayload = operationPayload;
            InitializeEntity();
        }
    }
}
