﻿using Notifications.Service.Domain.Email;
using Notifications.Service.Domain.Operation;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Notifications.Service.Repositories.Repositories.Email
{
    public interface IEmailNotificationRepository
    {
        Task<IEnumerable<EmailNotification>> GetAllNotificationsAsync();
        Task<IEnumerable<EmailNotification>> GetAllPendingEmailNotificationsAsync();
        Task<EmailNotification> GetByIdAsync(long id);
        Task CreateAsync(EmailNotification emailMetadata);
        Task UpdateNotificationAsync(EmailNotification emailMetadata, IEnumerable<Domain.Email.Email> unusedMailsToRemove = null);
        Task<OperationStatusEnum> GetNotificationStatusAsync(long id);
        Task<EmailNotification> GetNotificationDetailsAsync(long id);
    }
}