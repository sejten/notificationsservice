﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AddRequiredFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata");

            migrationBuilder.DropColumn(
                name: "SenderEmail",
                table: "EmailSender");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "EmailRecipient");

            migrationBuilder.AlterColumn<byte>(
                name: "OperationStatusId",
                table: "OperationMetadata",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OperationType",
                table: "OperationHistorie",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OperationPayload",
                table: "OperationHistorie",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OperationInfo",
                table: "OperationHistorie",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SenderEmailId",
                table: "EmailSender",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EmailId",
                table: "EmailRecipient",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "SenderId",
                table: "EmailMetadata",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Email",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailAddress = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Email", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailSender_SenderEmailId",
                table: "EmailSender",
                column: "SenderEmailId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailRecipient_EmailId",
                table: "EmailRecipient",
                column: "EmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata",
                column: "SenderId",
                principalTable: "EmailSender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailRecipient_Email_EmailId",
                table: "EmailRecipient",
                column: "EmailId",
                principalTable: "Email",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailSender_Email_SenderEmailId",
                table: "EmailSender",
                column: "SenderEmailId",
                principalTable: "Email",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId",
                principalTable: "OperationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailRecipient_Email_EmailId",
                table: "EmailRecipient");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailSender_Email_SenderEmailId",
                table: "EmailSender");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata");

            migrationBuilder.DropTable(
                name: "Email");

            migrationBuilder.DropIndex(
                name: "IX_EmailSender_SenderEmailId",
                table: "EmailSender");

            migrationBuilder.DropIndex(
                name: "IX_EmailRecipient_EmailId",
                table: "EmailRecipient");

            migrationBuilder.DropColumn(
                name: "SenderEmailId",
                table: "EmailSender");

            migrationBuilder.DropColumn(
                name: "EmailId",
                table: "EmailRecipient");

            migrationBuilder.AlterColumn<byte>(
                name: "OperationStatusId",
                table: "OperationMetadata",
                type: "tinyint",
                nullable: true,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<string>(
                name: "OperationType",
                table: "OperationHistorie",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "OperationPayload",
                table: "OperationHistorie",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "OperationInfo",
                table: "OperationHistorie",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "SenderEmail",
                table: "EmailSender",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "EmailRecipient",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SenderId",
                table: "EmailMetadata",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata",
                column: "SenderId",
                principalTable: "EmailSender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId",
                principalTable: "OperationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
