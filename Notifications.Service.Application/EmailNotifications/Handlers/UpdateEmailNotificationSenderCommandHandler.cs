﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Exceptions;
using Notifications.Service.Application.EmailNotifications.Extensions;
using Notifications.Service.Repositories.Repositories.Email;
using System.Threading;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Handlers
{
    public sealed class UpdateEmailNotificationSenderCommandHandler
        : IRequestHandler<UpdateEmailNotificationSenderCommand>
    {
        private readonly IEmailNotificationRepository _emailMetadataRepository;

        public UpdateEmailNotificationSenderCommandHandler(IEmailNotificationRepository emailMetadataRepository)
        {
            this._emailMetadataRepository = emailMetadataRepository;
        }

        public async Task<Unit> Handle(UpdateEmailNotificationSenderCommand request, CancellationToken cancellationToken)
        {
            if (!ShouldSkipValidation(request.Sender))
            {
                if (!request.Sender.IsValidEmailAddress())
                    throw new BusinessLogicException($"Given Email Sender is not valid email address");
            }

            var notification = await _emailMetadataRepository.GetByIdAsync(request.NotificationId);

            if (notification == null)
                throw new BusinessLogicException($"Notification with given Id {request.NotificationId} not found");

            if (notification.NotificationHasBeenSend())
                throw new BusinessLogicException($"Notification with given Id {request.NotificationId} is is already send");

            notification.UpdateSender(request?.Sender);

            await _emailMetadataRepository.UpdateNotificationAsync(notification);

            return Unit.Value;
        }

        private bool ShouldSkipValidation(string sender)
        {
            return string.IsNullOrWhiteSpace(sender);
        }
    }
}
