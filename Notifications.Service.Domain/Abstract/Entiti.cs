﻿using System;

namespace Notifications.Service.Domain.Abstract
{
    public abstract class Entity<T>
    {
        public T Id { get; private set; }
        public DateTime CreateDate { get; private set; }
        public DateTime ModificationDate { get; private set; }

        protected virtual void InitializeEntity()
        {
            CreateDate = DateTime.UtcNow;
            ModificationDate = DateTime.UtcNow;
        }

        protected virtual void EntityModified()
        {
            ModificationDate = DateTime.UtcNow;
        }
    }
}
