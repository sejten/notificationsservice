﻿using Notifications.Service.Domain.Operation;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Responses
{
    public class NotificationDetailsResponse
    {
        public string Sender { get; set; }
        public IEnumerable<string> Recipients { get; set; }
        public string NotificationContent { get; set; }
        public OperationStatusEnum NotificationStatus { get; set; }
    }
}
