﻿using Notifications.Service.Domain.Abstract;
using Notifications.Service.Domain.Constants;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace Notifications.Service.Domain.Email
{
    public class EmailNotification : EventableEntity<long>
    {
        public string EmailContent { get; private set; }
        public string Sender { get; private set; }
        public ICollection<Email> Recipients { get; private set; }

        private EmailNotification() {}
        public EmailNotification(
            string content, 
            string sender,
            IEnumerable<string> recipients)
        {
            EmailContent = content;
            Sender = sender;
            Recipients = recipients?
                .Distinct()
                .Select(recipient => new Email(recipient))
                .ToList();

            InitializeEventableEntity();

            Operation.InitializeHistory(
                nameof(EmailNotification),
                EmailNotifiations.InitializeNotificationStatus, 
                JsonSerializer.Serialize(this));
        }

        public virtual void UpdateSender(string sender)
        {
            if (!Sender.Equals(sender))
            {
                Sender = sender;

                Operation.AddHistory(nameof(EmailNotification),
                    EmailNotifiations.UpdatedNotificationSender,
                    JsonSerializer.Serialize(this));

                EntityModified();
            }                
        }
        public virtual IEnumerable<Email> GetItemsToRemove(IEnumerable<string> recipientsEmails)
        {
            List<Email> toRemove = new List<Email>();

            foreach (var dbRecipient in Recipients)
            {
                if (!recipientsEmails.Any(email => email == dbRecipient.EmailAddress))
                {
                    toRemove.Add(dbRecipient);
                }
            }

            return toRemove;
        }

        public virtual void RemoveRecipients(IEnumerable<Email> itemsToRemove)
        {
            bool removedItem = false;
            foreach (var toRemove in itemsToRemove)
            {
                var recipient = Recipients.FirstOrDefault(x => x.EmailAddress == toRemove.EmailAddress && x.Id == toRemove.Id);
                if (recipient != null)
                {
                    Recipients.Remove(recipient);
                    removedItem = true;
                }
            };

            if (itemsToRemove.Any() && removedItem)
            {
                Operation.AddHistory(nameof(EmailNotification),
                    EmailNotifiations.RemovedNotificationRecipients,
                    JsonSerializer.Serialize(this));

                EntityModified();
            }
        }

        public virtual void UpdateRecipients(IEnumerable<string> recipientsEmails)
        {
            foreach (var recipientEmail in recipientsEmails.Distinct())
            {
                if (!RecipientExists(recipientEmail))
                        Recipients.Add(new Email(recipientEmail));
            };

            Operation.AddHistory(nameof(EmailNotification),
                EmailNotifiations.UpdatedNotificationRecipients,
                JsonSerializer.Serialize(this));

            EntityModified();
        }

        public virtual void SendNotification()
        {
            if (Operation.OperationStatusId != Domain.Operation.OperationStatusEnum.Send)
            {
                Operation.SetOperationStatus(Domain.Operation.OperationStatusEnum.Send);

                Operation.AddHistory(nameof(EmailNotification),
                    EmailNotifiations.NotificationHasBeenSended,
                    JsonSerializer.Serialize(this));

                EntityModified();
            }
        }

        private bool RecipientExists(string recipient)
        {
            return Recipients.Any(x => x.EmailAddress == recipient);
        }
    }
}
