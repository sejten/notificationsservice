﻿using MediatR;

namespace Notifications.Service.Application.EmailNotifications.Commands
{
    public sealed class UpdateEmailNotificationSenderCommand : IRequest
    {
        public long NotificationId { get; }
        public string Sender { get; }

        public UpdateEmailNotificationSenderCommand(
            long notificationId,
            string sender)
        {
            NotificationId = notificationId;
            Sender = sender;
        }
    }
}
