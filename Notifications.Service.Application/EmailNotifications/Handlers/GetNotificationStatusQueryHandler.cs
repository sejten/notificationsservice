﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Queries;
using Notifications.Service.Domain.Operation;
using Notifications.Service.Repositories.Repositories.Email;
using System.Threading;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Handlers
{
    public sealed class GetNotificationStatusQueryHandler
        : IRequestHandler<GetNotificationStatusQuery, OperationStatusEnum>
    {
        private readonly IEmailNotificationRepository _emailNotificationRepository;

        public GetNotificationStatusQueryHandler(IEmailNotificationRepository emailNotificationRepository)
        {
            this._emailNotificationRepository = emailNotificationRepository;
        }

        public async Task<OperationStatusEnum> Handle(GetNotificationStatusQuery request, CancellationToken cancellationToken)
        {
            return await _emailNotificationRepository.GetNotificationStatusAsync(request.NotificationId);
        }
    }
}
