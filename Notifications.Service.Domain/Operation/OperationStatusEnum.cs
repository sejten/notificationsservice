﻿namespace Notifications.Service.Domain.Operation
{
    public enum OperationStatusEnum : byte
    {
        Pending,
        Send,
        Failed
    }
}
