﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AlterTablesNameToNotPlural : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadatas_OperationMetadatas_OperationId",
                table: "EmailMetadatas");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadatas_EmailSender_SenderId",
                table: "EmailMetadatas");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailRecipients_EmailMetadatas_EmailMetadataId",
                table: "EmailRecipients");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationHistories_OperationMetadatas_OperationMetadataId",
                table: "OperationHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadatas_OperationStatuses_OperationStatusId",
                table: "OperationMetadatas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationStatuses",
                table: "OperationStatuses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationMetadatas",
                table: "OperationMetadatas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationHistories",
                table: "OperationHistories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailRecipients",
                table: "EmailRecipients");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailMetadatas",
                table: "EmailMetadatas");

            migrationBuilder.RenameTable(
                name: "OperationStatuses",
                newName: "OperationStatuse");

            migrationBuilder.RenameTable(
                name: "OperationMetadatas",
                newName: "OperationMetadata");

            migrationBuilder.RenameTable(
                name: "OperationHistories",
                newName: "OperationHistorie");

            migrationBuilder.RenameTable(
                name: "EmailRecipients",
                newName: "EmailRecipient");

            migrationBuilder.RenameTable(
                name: "EmailMetadatas",
                newName: "EmailMetadata");

            migrationBuilder.RenameIndex(
                name: "IX_OperationMetadatas_OperationStatusId",
                table: "OperationMetadata",
                newName: "IX_OperationMetadata_OperationStatusId");

            migrationBuilder.RenameIndex(
                name: "IX_OperationHistories_OperationMetadataId",
                table: "OperationHistorie",
                newName: "IX_OperationHistorie_OperationMetadataId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailRecipients_EmailMetadataId",
                table: "EmailRecipient",
                newName: "IX_EmailRecipient_EmailMetadataId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailMetadatas_SenderId",
                table: "EmailMetadata",
                newName: "IX_EmailMetadata_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailMetadatas_OperationId",
                table: "EmailMetadata",
                newName: "IX_EmailMetadata_OperationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationStatuse",
                table: "OperationStatuse",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationMetadata",
                table: "OperationMetadata",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationHistorie",
                table: "OperationHistorie",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailRecipient",
                table: "EmailRecipient",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailMetadata",
                table: "EmailMetadata",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_OperationMetadata_OperationId",
                table: "EmailMetadata",
                column: "OperationId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata",
                column: "SenderId",
                principalTable: "EmailSender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailRecipient_EmailMetadata_EmailMetadataId",
                table: "EmailRecipient",
                column: "EmailMetadataId",
                principalTable: "EmailMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationHistorie_OperationMetadata_OperationMetadataId",
                table: "OperationHistorie",
                column: "OperationMetadataId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadata_OperationStatuse_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId",
                principalTable: "OperationStatuse",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_OperationMetadata_OperationId",
                table: "EmailMetadata");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailRecipient_EmailMetadata_EmailMetadataId",
                table: "EmailRecipient");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationHistorie_OperationMetadata_OperationMetadataId",
                table: "OperationHistorie");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadata_OperationStatuse_OperationStatusId",
                table: "OperationMetadata");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationStatuse",
                table: "OperationStatuse");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationMetadata",
                table: "OperationMetadata");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationHistorie",
                table: "OperationHistorie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailRecipient",
                table: "EmailRecipient");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailMetadata",
                table: "EmailMetadata");

            migrationBuilder.RenameTable(
                name: "OperationStatuse",
                newName: "OperationStatuses");

            migrationBuilder.RenameTable(
                name: "OperationMetadata",
                newName: "OperationMetadatas");

            migrationBuilder.RenameTable(
                name: "OperationHistorie",
                newName: "OperationHistories");

            migrationBuilder.RenameTable(
                name: "EmailRecipient",
                newName: "EmailRecipients");

            migrationBuilder.RenameTable(
                name: "EmailMetadata",
                newName: "EmailMetadatas");

            migrationBuilder.RenameIndex(
                name: "IX_OperationMetadata_OperationStatusId",
                table: "OperationMetadatas",
                newName: "IX_OperationMetadatas_OperationStatusId");

            migrationBuilder.RenameIndex(
                name: "IX_OperationHistorie_OperationMetadataId",
                table: "OperationHistories",
                newName: "IX_OperationHistories_OperationMetadataId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailRecipient_EmailMetadataId",
                table: "EmailRecipients",
                newName: "IX_EmailRecipients_EmailMetadataId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailMetadata_SenderId",
                table: "EmailMetadatas",
                newName: "IX_EmailMetadatas_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailMetadata_OperationId",
                table: "EmailMetadatas",
                newName: "IX_EmailMetadatas_OperationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationStatuses",
                table: "OperationStatuses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationMetadatas",
                table: "OperationMetadatas",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationHistories",
                table: "OperationHistories",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailRecipients",
                table: "EmailRecipients",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailMetadatas",
                table: "EmailMetadatas",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadatas_OperationMetadatas_OperationId",
                table: "EmailMetadatas",
                column: "OperationId",
                principalTable: "OperationMetadatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadatas_EmailSender_SenderId",
                table: "EmailMetadatas",
                column: "SenderId",
                principalTable: "EmailSender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailRecipients_EmailMetadatas_EmailMetadataId",
                table: "EmailRecipients",
                column: "EmailMetadataId",
                principalTable: "EmailMetadatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationHistories_OperationMetadatas_OperationMetadataId",
                table: "OperationHistories",
                column: "OperationMetadataId",
                principalTable: "OperationMetadatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadatas_OperationStatuses_OperationStatusId",
                table: "OperationMetadatas",
                column: "OperationStatusId",
                principalTable: "OperationStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
