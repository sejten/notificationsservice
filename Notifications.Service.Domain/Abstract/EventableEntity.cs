﻿using Notifications.Service.Domain.Operation;

namespace Notifications.Service.Domain.Abstract
{
    public abstract class EventableEntity<T> : Entity<T>
    {
        public Operation.Operation Operation { get; private set; }

        protected virtual void InitializeEventableEntity()
        {
            base.InitializeEntity();

            Operation = new Operation.Operation(OperationStatusEnum.Pending);
        }
    }
}
