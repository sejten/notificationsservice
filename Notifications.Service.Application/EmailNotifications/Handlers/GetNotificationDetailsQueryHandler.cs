﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Extensions;
using Notifications.Service.Application.EmailNotifications.Queries;
using Notifications.Service.Application.EmailNotifications.Responses;
using Notifications.Service.Repositories.Repositories.Email;
using System.Threading;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Handlers
{
    public sealed class GetNotificationDetailsQueryHandler
        : IRequestHandler<GetNotificationDetailsQuery, NotificationDetailsResponse>
    {
        private readonly IEmailNotificationRepository _emailNotificationRepository;

        public GetNotificationDetailsQueryHandler(IEmailNotificationRepository emailNotificationRepository)
        {
            this._emailNotificationRepository = emailNotificationRepository;
        }

        public async Task<NotificationDetailsResponse> Handle(GetNotificationDetailsQuery request, CancellationToken cancellationToken)
        {
            var notification = await _emailNotificationRepository.GetNotificationDetailsAsync(request.NotificationId);

            return notification?.ToDetailsResponse();
        }
    }
}
