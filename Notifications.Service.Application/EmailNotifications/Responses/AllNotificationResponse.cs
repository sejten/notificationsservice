﻿using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Responses
{
    public class AllNotificationResponse
    {
        public IEnumerable<NotificationResponse> NotificationResponses { get; set; }
    }

    public class NotificationResponse
    {
        public string Sender { get; set; }
        public IEnumerable<string> Recipients { get; set; }
        public string NotificationContent { get; set; }
    }
}
