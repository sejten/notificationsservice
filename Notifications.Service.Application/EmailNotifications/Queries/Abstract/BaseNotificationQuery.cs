﻿namespace Notifications.Service.Application.EmailNotifications.Queries.Abstract
{
    public abstract class BaseNotificationQuery
    {
        public long NotificationId { get; }

        public BaseNotificationQuery(long notificationId)
        {
            NotificationId = notificationId;
        }
    }
}
