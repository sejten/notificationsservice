﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class RmoveSenderIntoNotification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailNotification_EmailAddress_SenderId",
                table: "EmailNotification");

            migrationBuilder.DropIndex(
                name: "IX_EmailNotification_SenderId",
                table: "EmailNotification");

            migrationBuilder.DropColumn(
                name: "SenderId",
                table: "EmailNotification");

            migrationBuilder.AddColumn<string>(
                name: "Sender",
                table: "EmailNotification",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sender",
                table: "EmailNotification");

            migrationBuilder.AddColumn<int>(
                name: "SenderId",
                table: "EmailNotification",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EmailNotification_SenderId",
                table: "EmailNotification",
                column: "SenderId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailNotification_EmailAddress_SenderId",
                table: "EmailNotification",
                column: "SenderId",
                principalTable: "EmailAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
