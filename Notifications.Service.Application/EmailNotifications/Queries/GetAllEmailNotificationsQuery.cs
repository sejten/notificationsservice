﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Responses;

namespace Notifications.Service.Application.EmailNotifications.Queries
{
    public class GetAllEmailNotificationsQuery : IRequest<AllNotificationResponse>
    {
    }
}
