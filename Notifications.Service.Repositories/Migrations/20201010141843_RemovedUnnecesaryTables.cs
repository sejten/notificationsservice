﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class RemovedUnnecesaryTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata");

            migrationBuilder.DropTable(
                name: "EmailRecipient");

            migrationBuilder.DropTable(
                name: "EmailSender");

            migrationBuilder.AlterColumn<int>(
                name: "SenderId",
                table: "EmailMetadata",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "EmailAddress",
                table: "Email",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(80)");

            migrationBuilder.AddColumn<long>(
                name: "EmailNotificationId",
                table: "Email",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Email_EmailNotificationId",
                table: "Email",
                column: "EmailNotificationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Email_EmailMetadata_EmailNotificationId",
                table: "Email",
                column: "EmailNotificationId",
                principalTable: "EmailMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_Email_SenderId",
                table: "EmailMetadata",
                column: "SenderId",
                principalTable: "Email",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Email_EmailMetadata_EmailNotificationId",
                table: "Email");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_Email_SenderId",
                table: "EmailMetadata");

            migrationBuilder.DropIndex(
                name: "IX_Email_EmailNotificationId",
                table: "Email");

            migrationBuilder.DropColumn(
                name: "EmailNotificationId",
                table: "Email");

            migrationBuilder.AlterColumn<int>(
                name: "SenderId",
                table: "EmailMetadata",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EmailAddress",
                table: "Email",
                type: "nvarchar(80)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.CreateTable(
                name: "EmailRecipient",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmailId = table.Column<int>(type: "int", nullable: false),
                    EmailMetadataId = table.Column<long>(type: "bigint", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailRecipient", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailRecipient_Email_EmailId",
                        column: x => x.EmailId,
                        principalTable: "Email",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailRecipient_EmailMetadata_EmailMetadataId",
                        column: x => x.EmailMetadataId,
                        principalTable: "EmailMetadata",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailSender",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SenderEmailId = table.Column<int>(type: "int", nullable: false),
                    SenderName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailSender", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailSender_Email_SenderEmailId",
                        column: x => x.SenderEmailId,
                        principalTable: "Email",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailRecipient_EmailId",
                table: "EmailRecipient",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailRecipient_EmailMetadataId",
                table: "EmailRecipient",
                column: "EmailMetadataId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailSender_SenderEmailId",
                table: "EmailSender",
                column: "SenderEmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_EmailSender_SenderId",
                table: "EmailMetadata",
                column: "SenderId",
                principalTable: "EmailSender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
