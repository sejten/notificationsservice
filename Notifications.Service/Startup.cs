using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Notifications.Service.Api.Extensions;
using Notifications.Service.Application.Configuration;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Services;
using Notifications.Service.Application.EmailNotifications.Validators;
using Notifications.Service.Repositories;
using Notifications.Service.Repositories.Repositories.Email;

namespace Notifications.Service.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<NotificationsDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers();
            services.AddMediatR(typeof(CreateEmailNotificationCommand).GetTypeInfo().Assembly);
            services.AddTransient<IEmailNotificationRepository, EmailNotificationRepository>(); 

            services.AddSingleton<ISmtpSender, SmtpSender>();
            services.AddSingleton<IEmailNotificationValidator, NotificationSenderValidator>();
            services.AddSingleton<IEmailNotificationValidator, NotificationRecipientValidator>();

            services.AddSingleton<IEmailNotificationValidationProvider, EmailNotificationValidationProvider>();
            
            services.Configure<EmailConfiguration>(Configuration.GetSection(nameof(EmailConfiguration)));
            services.Configure<SmtpConfiguration>(Configuration.GetSection(nameof(SmtpConfiguration)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.ConfigureExceptionHandler();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
