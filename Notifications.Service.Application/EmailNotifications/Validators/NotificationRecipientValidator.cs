﻿using Microsoft.EntityFrameworkCore.Internal;
using Notifications.Service.Application.EmailNotifications.Extensions;
using Notifications.Service.Domain.Email;
using System;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Validators
{
    public sealed class NotificationRecipientValidator : IEmailNotificationValidator
    {
        public bool CanValidate(EmailNotification emailNotification)
        {
            return true;
        }

        public IEnumerable<Exception> Validate(EmailNotification emailNotification)
        {
            List<Exception> exceptions = new List<Exception>();

            if (emailNotification?.Recipients != null 
                && !emailNotification.Recipients.Any())
            {
                exceptions.Add(new Exception($"Email with Id {emailNotification.Id} has no recipients"));

                return exceptions;
            }

            foreach (var recipient in emailNotification.Recipients)
            {
                if (!recipient.EmailAddress.IsValidEmailAddress())
                    exceptions.Add(new Exception($"Email with Id {emailNotification.Id} has invalid recipient email {emailNotification.Sender}"));
            }

            return exceptions;
        }
    }
}
