﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AlterOperationStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata");

            migrationBuilder.DropIndex(
                name: "IX_OperationMetadata_OperationStatusId",
                table: "OperationMetadata");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_OperationMetadata_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId",
                principalTable: "OperationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
