﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AlterColumn_OperationStatus_Name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadata_OperationStatuse_OperationStatusId",
                table: "OperationMetadata");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationStatuse",
                table: "OperationStatuse");

            migrationBuilder.RenameTable(
                name: "OperationStatuse",
                newName: "OperationStatus");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "OperationStatus",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationStatus",
                table: "OperationStatus",
                column: "Id");

            migrationBuilder.InsertData(
                table: "OperationStatus",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)0, "Pending" });

            migrationBuilder.InsertData(
                table: "OperationStatus",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)1, "Send" });

            migrationBuilder.InsertData(
                table: "OperationStatus",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)2, "Failed" });

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId",
                principalTable: "OperationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OperationMetadata_OperationStatus_OperationStatusId",
                table: "OperationMetadata");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationStatus",
                table: "OperationStatus");

            migrationBuilder.DeleteData(
                table: "OperationStatus",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "OperationStatus",
                keyColumn: "Id",
                keyValue: (byte)1);

            migrationBuilder.DeleteData(
                table: "OperationStatus",
                keyColumn: "Id",
                keyValue: (byte)2);

            migrationBuilder.RenameTable(
                name: "OperationStatus",
                newName: "OperationStatuse");

            migrationBuilder.AlterColumn<int>(
                name: "Name",
                table: "OperationStatuse",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationStatuse",
                table: "OperationStatuse",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OperationMetadata_OperationStatuse_OperationStatusId",
                table: "OperationMetadata",
                column: "OperationStatusId",
                principalTable: "OperationStatuse",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
