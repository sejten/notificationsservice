﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Queries.Abstract;
using Notifications.Service.Domain.Operation;

namespace Notifications.Service.Application.EmailNotifications.Queries
{
    public sealed class GetNotificationStatusQuery : BaseNotificationQuery,
        IRequest<OperationStatusEnum>
    {
        public GetNotificationStatusQuery(long notificationId)
            : base(notificationId)
        {
        }
    }
}
