﻿namespace Notifications.Service.Application.Configuration
{
    public class EmailConfiguration
    {
        public string DefaultEmailSender { get; set; }
    }
}
