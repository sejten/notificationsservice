﻿using Moq;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Exceptions;
using Notifications.Service.Application.EmailNotifications.Handlers;
using Notifications.Service.Domain.Email;
using Notifications.Service.Repositories.Repositories.Email;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Notifications.Service.Application.Tests.EmailNotifications
{
    [TestFixture]
    public class UpdateEmailNotificationSenderCommandHandlerTests
    {
        private Mock<IEmailNotificationRepository> _emailMetadataRepository;
        private UpdateEmailNotificationSenderCommand _command;
        private UpdateEmailNotificationSenderCommandHandler _sut;

        [SetUp]
        public void Setup()
        {
            _emailMetadataRepository = new Mock<IEmailNotificationRepository>();

            _sut = new UpdateEmailNotificationSenderCommandHandler(_emailMetadataRepository.Object);
        }

        [Test]
        public async Task WhenUpdateEmailSender_WhenInvalidEmailHasBeenSended_ThenShouldThrowBusinessLogicException()
        {
            //arrange
            _command = new UpdateEmailNotificationSenderCommand(1, "invalidEmail");

            //act
            Assert.ThrowsAsync<BusinessLogicException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task WhenUpdateEmailSender_WhenNoNotificationFound_ThenShouldThrowBusinessLogicException()
        {
            //arrange
            _emailMetadataRepository.Setup(x => x.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(GetEmptynotificationResult()));

            _command = new UpdateEmailNotificationSenderCommand(1, "valid@email.com");
            //act
            Assert.ThrowsAsync<BusinessLogicException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task WhenUpdateEmailSender_WhenValidSenderAndNotificationInDatabase_ThenShouldupdateNotification()
        {
            //arrange
            _emailMetadataRepository.Setup(x => x.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(GetNotification()));

            _command = new UpdateEmailNotificationSenderCommand(1, "second@email.com");

            //act
            await _sut.Handle(_command, new System.Threading.CancellationToken());

            //assert
            _emailMetadataRepository.Verify(x => x.UpdateNotificationAsync(It.IsAny<EmailNotification>(),
                It.IsAny<IEnumerable<Email>>()), Times.Once);

        }

        private EmailNotification GetNotification()
        {
            return new EmailNotification("some content", "first@sender.com", new List<string>() { "sender@sender" });
        }

        private EmailNotification GetEmptynotificationResult()
        {
            return null; 
        }
    }
}
