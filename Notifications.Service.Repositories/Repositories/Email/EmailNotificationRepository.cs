﻿using Microsoft.EntityFrameworkCore;
using Notifications.Service.Domain.Email;
using Notifications.Service.Domain.Operation;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notifications.Service.Repositories.Repositories.Email
{
    public class EmailNotificationRepository : IEmailNotificationRepository
    {
        private readonly NotificationsDbContext _dbContext;

        public EmailNotificationRepository(NotificationsDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IEnumerable<EmailNotification>> GetAllNotificationsAsync()
        {
            return await _dbContext.EmailNotification
                .Include(notification => notification.Recipients)
                .OrderBy(notification => notification.CreateDate)
                .ToListAsync();
        }

        public async Task<IEnumerable<EmailNotification>> GetAllPendingEmailNotificationsAsync()
        {
            return await _dbContext.EmailNotification
                .Include(notification => notification.Recipients)
                .Where(notification => notification.Operation.OperationStatusId == OperationStatusEnum.Pending)
                .ToListAsync();
        }

        public async Task CreateAsync(EmailNotification emailMetadata)
        {
            _dbContext.EmailNotification.Add(emailMetadata);
            await _dbContext.SaveChangesAsync();
        }

        public Task<EmailNotification> GetByIdAsync(long id)
        {
            return _dbContext.EmailNotification
                .Include(notification => notification.Recipients)
                .Include(notification => notification.Operation)
                .ThenInclude(notification => notification.OperationHistory)
                .FirstOrDefaultAsync(notification => notification.Id == id);
        }

        public async Task UpdateNotificationAsync(
            EmailNotification emailMetadata,
            IEnumerable<Domain.Email.Email> unusedMailsToRemove = null)
        {
            _dbContext.EmailNotification.Attach(emailMetadata);

            if (ShouldRemoveUnusedMails(unusedMailsToRemove))
                _dbContext.EmailAddress.RemoveRange(unusedMailsToRemove);

            await _dbContext.SaveChangesAsync();
        }

        private static bool ShouldRemoveUnusedMails(IEnumerable<Domain.Email.Email> unusedMailsToRemove)
        {
            return unusedMailsToRemove != null && unusedMailsToRemove.Any();
        }

        public Task<OperationStatusEnum> GetNotificationStatusAsync(long id)
        {
            return _dbContext.EmailNotification
                .Include(notification => notification.Operation)
                .Where(notification => notification.Id == id)
                .Select(x => x.Operation.OperationStatusId)
                .SingleOrDefaultAsync();
        }

        public Task<EmailNotification> GetNotificationDetailsAsync(long id)
        {
            return _dbContext.EmailNotification
                .Include(notification => notification.Recipients)
                .Include(notification => notification.Operation)
                .FirstOrDefaultAsync(notification => notification.Id == id);
        }
    }
}
