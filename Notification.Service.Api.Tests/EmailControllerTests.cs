﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Notification.Service.Api.Tests
{
    public class EmailControllerTests : IClassFixture<AppTestFixture>
    {
        readonly AppTestFixture _fixture;
        readonly HttpClient _client;
        public EmailControllerTests(AppTestFixture fixture)
        {
            _fixture = fixture;
            _client = fixture.CreateClient();
        }

        [Theory]
        [InlineData("api/Email/CreateEmail")]
        public async Task Create_NewEmail_Success(string url)
        {
            // Act
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);

            requestMessage.Content = new StringContent(File.ReadAllText(@".\Data\CreateNotification.json"), Encoding.Default, "application/json");

            var response = await _client.PostAsync(url, requestMessage.Content);
            // Assert1
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Theory]
        [InlineData("api/Email/SendPendingNotifications")]
        public async Task Send_PendingEmails_Success(string url)
        {
            // Act
            HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, url);

            var response = await _client.PostAsync(url, requestMessage.Content);
            // Assert1
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
