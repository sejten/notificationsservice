﻿using System;

namespace Notifications.Service.Domain.Abstract
{
    public abstract class ImmutableEntity<T>
    {
        public T Id { get; private set; }
        public DateTime CreateDate { get; private set; }

        protected virtual void InitializeEntity()
        {
            CreateDate = DateTime.UtcNow;
        }
    }
}
