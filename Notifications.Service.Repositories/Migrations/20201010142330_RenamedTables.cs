﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class RenamedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Email_EmailMetadata_EmailNotificationId",
                table: "Email");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_OperationMetadata_OperationId",
                table: "EmailMetadata");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadata_Email_SenderId",
                table: "EmailMetadata");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationHistorie_OperationMetadata_OperationMetadataId",
                table: "OperationHistorie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationHistorie",
                table: "OperationHistorie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailMetadata",
                table: "EmailMetadata");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Email",
                table: "Email");

            migrationBuilder.RenameTable(
                name: "OperationHistorie",
                newName: "OperationHistory");

            migrationBuilder.RenameTable(
                name: "EmailMetadata",
                newName: "EmailNotification");

            migrationBuilder.RenameTable(
                name: "Email",
                newName: "EmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_OperationHistorie_OperationMetadataId",
                table: "OperationHistory",
                newName: "IX_OperationHistory_OperationMetadataId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailMetadata_SenderId",
                table: "EmailNotification",
                newName: "IX_EmailNotification_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailMetadata_OperationId",
                table: "EmailNotification",
                newName: "IX_EmailNotification_OperationId");

            migrationBuilder.RenameIndex(
                name: "IX_Email_EmailNotificationId",
                table: "EmailAddress",
                newName: "IX_EmailAddress_EmailNotificationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationHistory",
                table: "OperationHistory",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailNotification",
                table: "EmailNotification",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailAddress",
                table: "EmailAddress",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_EmailNotification_EmailNotificationId",
                table: "EmailAddress",
                column: "EmailNotificationId",
                principalTable: "EmailNotification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailNotification_OperationMetadata_OperationId",
                table: "EmailNotification",
                column: "OperationId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailNotification_EmailAddress_SenderId",
                table: "EmailNotification",
                column: "SenderId",
                principalTable: "EmailAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationHistory_OperationMetadata_OperationMetadataId",
                table: "OperationHistory",
                column: "OperationMetadataId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailAddress_EmailNotification_EmailNotificationId",
                table: "EmailAddress");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailNotification_OperationMetadata_OperationId",
                table: "EmailNotification");

            migrationBuilder.DropForeignKey(
                name: "FK_EmailNotification_EmailAddress_SenderId",
                table: "EmailNotification");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationHistory_OperationMetadata_OperationMetadataId",
                table: "OperationHistory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationHistory",
                table: "OperationHistory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailNotification",
                table: "EmailNotification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EmailAddress",
                table: "EmailAddress");

            migrationBuilder.RenameTable(
                name: "OperationHistory",
                newName: "OperationHistorie");

            migrationBuilder.RenameTable(
                name: "EmailNotification",
                newName: "EmailMetadata");

            migrationBuilder.RenameTable(
                name: "EmailAddress",
                newName: "Email");

            migrationBuilder.RenameIndex(
                name: "IX_OperationHistory_OperationMetadataId",
                table: "OperationHistorie",
                newName: "IX_OperationHistorie_OperationMetadataId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailNotification_SenderId",
                table: "EmailMetadata",
                newName: "IX_EmailMetadata_SenderId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailNotification_OperationId",
                table: "EmailMetadata",
                newName: "IX_EmailMetadata_OperationId");

            migrationBuilder.RenameIndex(
                name: "IX_EmailAddress_EmailNotificationId",
                table: "Email",
                newName: "IX_Email_EmailNotificationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationHistorie",
                table: "OperationHistorie",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EmailMetadata",
                table: "EmailMetadata",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Email",
                table: "Email",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Email_EmailMetadata_EmailNotificationId",
                table: "Email",
                column: "EmailNotificationId",
                principalTable: "EmailMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_OperationMetadata_OperationId",
                table: "EmailMetadata",
                column: "OperationId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadata_Email_SenderId",
                table: "EmailMetadata",
                column: "SenderId",
                principalTable: "Email",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationHistorie_OperationMetadata_OperationMetadataId",
                table: "OperationHistorie",
                column: "OperationMetadataId",
                principalTable: "OperationMetadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
