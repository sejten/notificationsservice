﻿using Microsoft.EntityFrameworkCore.Internal;
using Notifications.Service.Application.EmailNotifications.Validators;
using Notifications.Service.Domain.Email;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Notifications.Service.Application.EmailNotifications.Services
{
    public sealed class EmailNotificationValidationProvider : IEmailNotificationValidationProvider
    {
        private readonly IEnumerable<IEmailNotificationValidator> _emailNotificationValidators;

        public EmailNotificationValidationProvider(IEnumerable<IEmailNotificationValidator> emailNotificationValidators)
        {
            this._emailNotificationValidators = emailNotificationValidators;
        }
        public IEnumerable<Exception> Validate(EmailNotification emailNotification)
        {
            List<Exception> exceptionList = new List<Exception>();

            foreach(var validator in _emailNotificationValidators)
            {
                if (validator.CanValidate(emailNotification))
                {
                    var validationExceptions = validator.Validate(emailNotification);

                    if (validationExceptions.Any())
                        exceptionList.AddRange(validationExceptions);
                }
            }

            return exceptionList;
        }
    }
}
