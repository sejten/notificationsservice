﻿using Moq;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Exceptions;
using Notifications.Service.Application.EmailNotifications.Handlers;
using Notifications.Service.Domain.Email;
using Notifications.Service.Repositories.Repositories.Email;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notifications.Service.Application.Tests.EmailNotifications
{
    [TestFixture]
    public class UpdateEmailNotificationRecipientCommandHandlerTests
    {
        private Mock<IEmailNotificationRepository> _emailMetadataRepository;
        private UpdateEmailNotificationRecipientCommand _command;
        private UpdateEmailNotificationRecipientCommandHandler _sut;

        [SetUp]
        public void Setup()
        {
            _emailMetadataRepository = new Mock<IEmailNotificationRepository>();
            _sut = new UpdateEmailNotificationRecipientCommandHandler(_emailMetadataRepository.Object);
        }

        [Test]
        public async Task GivenUpdateRecipientAction_WhenAnyInvalidRecipientEmail_ThenShouldThrowException()
        {
            //arrange
            _command = new UpdateEmailNotificationRecipientCommand(1, new List<string>() { "invalid", "invalid2" });

            //act
            Assert.ThrowsAsync<BusinessLogicException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task GivenUpdateRecipientAction_WhenNoNotificationInDatabase_ThenShouldThrowException()
        {
            //arrange
            _emailMetadataRepository.Setup(x => x.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(GetEmptynotificationResult()));
            _command = new UpdateEmailNotificationRecipientCommand(1, new List<string>() { "test@test.pl" });

            //act
            Assert.ThrowsAsync<BusinessLogicException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task GivenUpdateRecipientAction_WhenNotificationValidAndInDatabase_ThenShouldUpdateNotificationInDatabase()
        {
            //arrange
            _emailMetadataRepository.Setup(x => x.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(GetNotification()));
            _command = new UpdateEmailNotificationRecipientCommand(1, new List<string>() { "sender@sender", "test@test.pl" });

            //act
            await _sut.Handle(_command, new System.Threading.CancellationToken());

            //assert
            _emailMetadataRepository.Verify(x => x.UpdateNotificationAsync(It.Is<EmailNotification>(y => y.Recipients.ToList().Count == 2), It.IsAny<IEnumerable<Email>>()), Times.Once);
        }

        private EmailNotification GetNotification()
        {
            return new EmailNotification("some content", "first@sender.com", new List<string>() { "sender@sender", "sender2@sender", "sender3@sender" });
        }

        private EmailNotification GetEmptynotificationResult()
        {
            return null;
        }
    }
}
