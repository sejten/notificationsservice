using Notifications.Service.Domain.Email;
using NUnit.Framework;
using System.Collections.Generic;

namespace Notifications.Service.Domain.Tests
{
    public class EmailNotificationTests
    {
        [Test]
        public void EmailNotification_Initialization_test()
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() { "test@test.pl", "test@test.pl" };

            //act
            var notification = new EmailNotification(content, sender, recipients);

            //assert
            Assert.That(notification.Recipients.Count == 1);
            Assert.That(notification.Operation.OperationStatusId == Operation.OperationStatusEnum.Pending);
            Assert.That(notification.Operation.OperationHistory.Count == 1);
        }

        [Test]
        [TestCase(null, 2)]
        [TestCase("sender@test.com", 1)]
        [TestCase("newSender@test.com", 2)]
        public void EmailNotification_UpdateSender_test(string newSender, int operationCounter)
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() { "test@test.pl" };

            //act
            var notification = new EmailNotification(content, sender, recipients);
            notification.UpdateSender(newSender);

            //assert
            Assert.That(notification.Operation.OperationHistory.Count == operationCounter);
            Assert.That(notification.Sender == newSender);

        }

        [Test]
        public void EmailNotification_UpdateRecipients_test()
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() { "test@test.pl" };
            var newRecipients = new List<string>() { "test@test.pl", "test2@test.pl", "test2@test.pl" };

            //act
            var notification = new EmailNotification(content, sender, recipients);
            notification.UpdateRecipients(newRecipients);

            //assert
            Assert.That(notification.Operation.OperationHistory.Count > 1);
            Assert.That(notification.Recipients.Count == 2);
        }

        [Test]
        public void EmailNotification_RemoveRecipients_test()
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() { "test@test.pl" };
            var toRemove = new List<Notifications.Service.Domain.Email.Email>() { new Email.Email("test@test.pl") };

            //act
            var notification = new EmailNotification(content, sender, recipients);
            notification.RemoveRecipients(toRemove);

            //assert
            Assert.That(notification.Operation.OperationHistory.Count > 1);
            Assert.That(notification.Recipients.Count == 0);
        }

        [Test]
        public void EmailNotification_RemoveEmptyListOfRecipients_test()
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() { "test@test.pl" };
            var toRemove = new List<Notifications.Service.Domain.Email.Email>();

            //act
            var notification = new EmailNotification(content, sender, recipients);
            notification.RemoveRecipients(toRemove);

            //assert
            Assert.That(notification.Operation.OperationHistory.Count == 1);
            Assert.That(notification.Recipients.Count == 1);
        }

        [Test]
        public void EmailNotification_RemoveRecipientsWhenNoRecipients_test()
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() ;
            var toRemove = new List<Notifications.Service.Domain.Email.Email>() { new Email.Email("test@test.pl") };

            //act
            var notification = new EmailNotification(content, sender, recipients);
            notification.RemoveRecipients(toRemove);

            //assert
            Assert.That(notification.Operation.OperationHistory.Count == 1);
            Assert.That(notification.Recipients.Count == 0);
        }

        [Test]
        public void EmailNotification_SendNotification_test()
        {
            //arrange
            var content = "content";
            var sender = "sender@test.com";
            var recipients = new List<string>() { "test@test.pl" };

            //act
            var notification = new EmailNotification(content, sender, recipients);
            notification.SendNotification();

            //assert
            Assert.That(notification.Operation.OperationHistory.Count == 2);
            Assert.That(notification.Operation.OperationStatusId == Operation.OperationStatusEnum.Send);
        }
    }
}