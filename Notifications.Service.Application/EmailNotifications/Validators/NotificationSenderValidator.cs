﻿using Notifications.Service.Application.EmailNotifications.Extensions;
using Notifications.Service.Domain.Email;
using System;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Validators
{
    public sealed class NotificationSenderValidator : IEmailNotificationValidator
    {
        public bool CanValidate(EmailNotification emailNotification)
        {
            return !string.IsNullOrWhiteSpace(emailNotification.Sender);
        }

        public IEnumerable<Exception> Validate(EmailNotification emailNotification)
        {
            List<Exception> exceptions = new List<Exception>();

            if (!emailNotification.Sender.IsValidEmailAddress())
                exceptions.Add(new Exception($"Email with Id {emailNotification.Id} has invalid sender email {emailNotification.Sender}"));

            return exceptions;
        }
    }
}
