﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Services
{
    public interface ISmtpSender
    {
        public Task SendAsync(string from, IEnumerable<string> to, string body);
    }
}
