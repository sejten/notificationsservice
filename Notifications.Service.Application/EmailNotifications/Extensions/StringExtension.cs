﻿namespace Notifications.Service.Application.EmailNotifications.Extensions
{
    public static class StringExtension
    {
        public static bool IsValidEmailAddress(this string emailAddress)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(emailAddress);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
