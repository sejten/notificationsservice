﻿namespace Notifications.Service.Api.Models
{
    public class UpdateSenderDto
    {
        public string Sender { get; set; }
    }
}