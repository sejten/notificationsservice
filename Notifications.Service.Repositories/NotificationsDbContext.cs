﻿using Microsoft.EntityFrameworkCore;
using Notifications.Service.Domain.Email;
using Notifications.Service.Domain.Operation;
using System;
using System.Linq;

namespace Notifications.Service.Repositories
{
    public class NotificationsDbContext : DbContext
    {
        public NotificationsDbContext(DbContextOptions<NotificationsDbContext> options)
            : base(options)
        {
        }
        public DbSet<EmailNotification> EmailNotification { get; set; }
        public DbSet<Email> EmailAddress { get; set; }
        public DbSet<OperationHistory> OperationHistory { get; set; }
        public DbSet<Operation> Operation { get; set; }
        public DbSet<OperationStatus> OperationStatus { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<OperationStatus>()
                .Property(e => e.Id)
                .HasConversion<byte>();

            modelBuilder
                .Entity<OperationStatus>().HasData(
                    Enum.GetValues(typeof(OperationStatusEnum))
                        .Cast<OperationStatusEnum>()
                        .Select(e => new OperationStatus(e, e.ToString()))
                );
        }

    }
}
