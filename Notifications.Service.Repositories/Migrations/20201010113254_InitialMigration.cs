﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "OperationStatuses",
                columns: table => new
                {
                    Id = table.Column<byte>(nullable: false),
                    Name = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OperationMetadatas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModificationDate = table.Column<DateTime>(nullable: false),
                    OperationStatusId = table.Column<byte>(nullable: true),
                    StatusId = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationMetadatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperationMetadatas_OperationStatuses_OperationStatusId",
                        column: x => x.OperationStatusId,
                        principalTable: "OperationStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailMetadatas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModificationDate = table.Column<DateTime>(nullable: false),
                    OperationId = table.Column<long>(nullable: true),
                    EmailContent = table.Column<string>(nullable: true),
                    Sender = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailMetadatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailMetadatas_OperationMetadatas_OperationId",
                        column: x => x.OperationId,
                        principalTable: "OperationMetadatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OperationHistories",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    OperationType = table.Column<string>(nullable: true),
                    OperationInfo = table.Column<string>(nullable: true),
                    OperationPayload = table.Column<string>(nullable: true),
                    OperationMetadataId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperationHistories_OperationMetadatas_OperationMetadataId",
                        column: x => x.OperationMetadataId,
                        principalTable: "OperationMetadatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailRecipients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EmailMetadataId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailRecipients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailRecipients_EmailMetadatas_EmailMetadataId",
                        column: x => x.EmailMetadataId,
                        principalTable: "EmailMetadatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailMetadatas_OperationId",
                table: "EmailMetadatas",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailRecipients_EmailMetadataId",
                table: "EmailRecipients",
                column: "EmailMetadataId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationHistories_OperationMetadataId",
                table: "OperationHistories",
                column: "OperationMetadataId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationMetadatas_OperationStatusId",
                table: "OperationMetadatas",
                column: "OperationStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailRecipients");

            migrationBuilder.DropTable(
                name: "OperationHistories");

            migrationBuilder.DropTable(
                name: "EmailMetadatas");

            migrationBuilder.DropTable(
                name: "OperationMetadatas");

            migrationBuilder.DropTable(
                name: "OperationStatuses");
        }
    }
}
