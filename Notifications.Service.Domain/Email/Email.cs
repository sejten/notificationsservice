﻿using System.ComponentModel.DataAnnotations;

namespace Notifications.Service.Domain.Email
{
    public class Email
    {
        public int Id { get; private set; }

        [Required]
        public string EmailAddress { get; private set; }

        private Email() {}

        public Email(string emailAddress)
        {
            EmailAddress = emailAddress;
        }
    }
}
