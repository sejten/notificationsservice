﻿using MediatR;
using Microsoft.Extensions.Options;
using Notifications.Service.Application.Configuration;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Services;
using Notifications.Service.Repositories.Repositories.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Handlers
{
    public sealed class SendPendingEmailsCommandHandler : IRequestHandler<SendPendingEmailsCommand>
    {
        private readonly IEmailNotificationRepository _emailNotificationRepository;
        private readonly IEmailNotificationValidationProvider _emailNotificationValidationProvider;
        private readonly ISmtpSender _smtpSender;
        private readonly EmailConfiguration _emailOptions;

        public SendPendingEmailsCommandHandler(
            IEmailNotificationRepository emailNotificationRepository,
            IEmailNotificationValidationProvider emailNotificationValidationProvider,
            ISmtpSender smtpSender,
            IOptions<EmailConfiguration> emailOptions)
        {
            this._emailNotificationRepository = emailNotificationRepository;
            this._emailNotificationValidationProvider = emailNotificationValidationProvider;
            this._smtpSender = smtpSender;
            this._emailOptions = emailOptions.Value;
        }

        public async Task<Unit> Handle(SendPendingEmailsCommand request, CancellationToken cancellationToken)
        {
            var allPendingNotifications = await _emailNotificationRepository.GetAllPendingEmailNotificationsAsync();

            var exceptionlist = new List<Exception>();

            foreach (var notification in allPendingNotifications)
            {
                var validationExceptions = _emailNotificationValidationProvider.Validate(notification);

                if (!validationExceptions.Any())
                {
                    await _smtpSender.SendAsync(
                        from: string.IsNullOrWhiteSpace(notification.Sender)
                            ? _emailOptions.DefaultEmailSender
                            : notification.Sender,
                        to: notification.Recipients.Select(x => x.EmailAddress),
                        body: notification?.EmailContent);

                    var emailNotification = await _emailNotificationRepository.GetByIdAsync(notification.Id);

                    emailNotification.SendNotification();

                    await _emailNotificationRepository.UpdateNotificationAsync(emailNotification);
                }
                else
                {
                    exceptionlist.AddRange(validationExceptions);
                }
            }

            if (exceptionlist.Any())
                throw new AggregateException(exceptionlist);

            return Unit.Value;
        }

    }
}
