﻿using Microsoft.Extensions.Options;
using Notifications.Service.Application.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Services
{
    public sealed class SmtpSender : ISmtpSender
    {
        private readonly SmtpConfiguration smtpOptions;

        public SmtpSender(IOptions<SmtpConfiguration> smtpOptions)
        {
            this.smtpOptions = smtpOptions.Value;
        }
        public async Task SendAsync(string from, IEnumerable<string> recipients, string body)
        {
            if (!smtpOptions.SendingEmailsEnabled)
                return;

            var email = new MailMessage()
            {
                From = new MailAddress(from),
                Body = body
            };

            foreach (var recipient in recipients)
                email.To.Add(recipient);

            email.Headers.Add("Content-Type", "content=text/html; charset=\"UTF-8\"");


            using (var smtp = new SmtpClient(smtpOptions.SmtpHost, smtpOptions.Port))
            {
                smtp.Credentials = new NetworkCredential(smtpOptions.Username, smtpOptions.Password);
                await smtp.SendMailAsync(email);
            }
        }
    }
}
