﻿using Moq;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Exceptions;
using Notifications.Service.Application.EmailNotifications.Handlers;
using Notifications.Service.Domain.Email;
using Notifications.Service.Repositories.Repositories.Email;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Notifications.Service.Application.Tests.EmailNotifications
{
    public class CreateEmailNotificationCommandHandlerTests
    {
        private CreateEmailNotificationCommandHandler _sut;
        private CreateEmailNotificationCommand _command;
        private Mock<IEmailNotificationRepository> _emailNotificationRepository;

        [SetUp]
        public void Setup()
        {
            _emailNotificationRepository = new Mock<IEmailNotificationRepository>();
            _sut = new CreateEmailNotificationCommandHandler(_emailNotificationRepository.Object);
        }

        [Test]
        public async Task GivenCreateEmailNotification_WhenSenderPassedInvalidEmail_ShouldThrowException()
        {
            //arrange
            _command = new CreateEmailNotificationCommand("some content", new List<string>(), "invalidEmail");

            //act
            Assert.ThrowsAsync<BusinessLogicException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task GivenCreateEmailNotification_WhenRecipientHasInvalidEmail_ShouldThrowException()
        {
            //arrange
            _command = new CreateEmailNotificationCommand("some content", new List<string>() { "invalidEmail" }, null);

            //act
            Assert.ThrowsAsync<BusinessLogicException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task GivenCreateEmailNotification_WhenValidDataInRequest_ShouldCreateNewNotification()
        {
            //arrange
            _command = new CreateEmailNotificationCommand("some content", new List<string>(), null);

            //act
            await _sut.Handle(_command, new System.Threading.CancellationToken());

            //assert
            _emailNotificationRepository.Verify(x => x.CreateAsync(It.IsAny<EmailNotification>()), Times.Once);
        }
    }
}
