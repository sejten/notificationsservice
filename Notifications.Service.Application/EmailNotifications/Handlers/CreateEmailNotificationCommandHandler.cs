﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Exceptions;
using Notifications.Service.Application.EmailNotifications.Extensions;
using Notifications.Service.Domain.Email;
using Notifications.Service.Repositories.Repositories.Email;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Notifications.Service.Application.EmailNotifications.Handlers
{
    public sealed class CreateEmailNotificationCommandHandler 
        : IRequestHandler<CreateEmailNotificationCommand>
    {
        private readonly IEmailNotificationRepository _emailMetadataRepository;

        public CreateEmailNotificationCommandHandler(IEmailNotificationRepository emailMetadataRepository)
        {
            this._emailMetadataRepository = emailMetadataRepository;
        }
        public async Task<Unit> Handle(CreateEmailNotificationCommand request, CancellationToken cancellationToken)
        {
            ValidateRequest(request);

            var emailNotification = 
                new EmailNotification(request.EmailContent, request.Sender, request?.Recipients);

            await _emailMetadataRepository.CreateAsync(emailNotification);

            return Unit.Value;
        }

        private void ValidateRequest(CreateEmailNotificationCommand request)
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (!ShouldSkipValidation(request.Sender))
            {
                if (!request.Sender.IsValidEmailAddress())
                    stringBuilder.Append($"Given Email Sender is not valid email address");
            }


            foreach (var recipient in request.Recipients)
            {
                if (!recipient.IsValidEmailAddress())
                    stringBuilder.Append($"Given recipient email {recipient} is not valid email. ");
            }

            if (stringBuilder.Length > 0)
            {
                throw new BusinessLogicException(stringBuilder.ToString());
            }
        }

        private bool ShouldSkipValidation(string sender)
        {
            return string.IsNullOrWhiteSpace(sender);
        }
    }
}
