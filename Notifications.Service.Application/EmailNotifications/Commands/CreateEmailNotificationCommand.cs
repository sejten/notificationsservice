﻿using MediatR;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Commands
{
    public sealed class CreateEmailNotificationCommand : IRequest
    {
        public string EmailContent { get; }
        public IEnumerable<string> Recipients { get; }
        public string Sender { get; }

        public CreateEmailNotificationCommand(
            string emailContent,
            IEnumerable<string> recipients,
            string sender)
        {
            EmailContent = emailContent;
            Recipients = recipients;
            Sender = sender;
        }
    }
}
