﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AlterTableOperationMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "OperationMetadata");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "StatusId",
                table: "OperationMetadata",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);
        }
    }
}
