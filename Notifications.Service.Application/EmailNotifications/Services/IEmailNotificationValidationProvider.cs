﻿using Notifications.Service.Domain.Email;
using System;
using System.Collections.Generic;

namespace Notifications.Service.Application.EmailNotifications.Services
{
    public interface IEmailNotificationValidationProvider
    {
        IEnumerable<Exception> Validate(EmailNotification emailNotifications);
    }
}
