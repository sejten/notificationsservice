﻿using System.Collections.Generic;

namespace Notifications.Service.Api.Models
{
    public class UpdateRecipientsDto
    {
        public IEnumerable<EmailRecipientDto> EmailRecipients { get; set; }
    }
}
