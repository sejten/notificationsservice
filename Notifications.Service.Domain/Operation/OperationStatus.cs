﻿namespace Notifications.Service.Domain.Operation
{
    public class OperationStatus
    {
        public OperationStatusEnum Id { get; private set; }
        public string Name { get; private set; }

        private OperationStatus(){}

        public OperationStatus(OperationStatusEnum operationStatusEnum, string name = "")
        {
            Id = operationStatusEnum;
            Name = operationStatusEnum.ToString();
        }

    }
}
