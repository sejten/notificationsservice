﻿using System.Collections.Generic;

namespace Notifications.Service.Api.Models
{
    public class CreateEmailDto
    {
        public string Content { get; set; }

        public IEnumerable<EmailRecipientDto> EmailRecipients { get; set; }

        public string EmailSender { get; set; }
    }
}
