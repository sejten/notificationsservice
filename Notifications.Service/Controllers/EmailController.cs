﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Notifications.Service.Api.Models;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Queries;
using System.Linq;
using System.Threading.Tasks;

namespace Notifications.Service.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController] 
    public class EmailController : ControllerBase
    {
        private readonly IMediator _bus;

        public EmailController(IMediator bus)
        {
            this._bus = bus;
        }

        [HttpPost("CreateEmail")]
        public async Task<IActionResult> Post([FromBody] CreateEmailDto emailDto)
            => Ok(await _bus.Send(
                      new CreateEmailNotificationCommand(
                          emailContent: emailDto.Content,
                          recipients: emailDto.EmailRecipients?.Select(x => x.Email),
                          sender: emailDto.EmailSender)
                  ));

        [HttpPut("ReDefineRecipient/{id}")]
        public async Task<IActionResult> Update(long id, [FromBody] UpdateRecipientsDto updateRecipientsDto)
            => Ok(await _bus.Send(
                      new UpdateEmailNotificationRecipientCommand(
                          notificationId: id,
                          recipients: updateRecipientsDto.EmailRecipients.Select(x => x.Email))
                  ));

        [HttpPut("ReDefineSender/{id}")]
        public async Task<IActionResult> Update(long id, [FromBody] UpdateSenderDto updateSenderDto)
            => Ok(await _bus.Send(new UpdateEmailNotificationSenderCommand(
                      notificationId: id,
                      sender: updateSenderDto.Sender)
                  ));

        [HttpGet("NotificationStatus")]
        public async Task<IActionResult> GetStatus([FromQuery] long notificationId)
            => Ok(await _bus.Send(new GetNotificationStatusQuery(
                      notificationId: notificationId)
                  ));

        [HttpGet("NotificationDetails")]
        public async Task<IActionResult> GetDetails([FromQuery] long notificationId)
            => Ok(await _bus.Send(new GetNotificationDetailsQuery(
                      notificationId: notificationId)
                  ));

        [HttpGet("AllNotifications")]
        public async Task<IActionResult> GetAll()
            => Ok(await _bus.Send(new GetAllEmailNotificationsQuery()));

        [HttpPost("SendPendingNotifications")]
        public async Task<IActionResult> Post()
            => Ok(await _bus.Send(new SendPendingEmailsCommand()));
    }
}
