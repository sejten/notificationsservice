﻿using MediatR;
using Notifications.Service.Application.EmailNotifications.Queries.Abstract;
using Notifications.Service.Application.EmailNotifications.Responses;

namespace Notifications.Service.Application.EmailNotifications.Queries
{
    public sealed class GetNotificationDetailsQuery : BaseNotificationQuery,
        IRequest<NotificationDetailsResponse>
    {
        public GetNotificationDetailsQuery(long notificationId)
            : base(notificationId)
        {
        }
    }
}
