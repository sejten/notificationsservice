﻿using Notifications.Service.Domain.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Notifications.Service.Domain.Operation
{
    public class Operation : Entity<long>
    {
        [Required]
        public OperationStatusEnum OperationStatusId { get; private set; }
        public ICollection<OperationHistory> OperationHistory { get; private set; }

        private Operation() { }

        public Operation(OperationStatusEnum operationStatusEnum)
        {
            InitializeEntity();
            InitializeOperations(operationStatusEnum);
        }

        protected virtual void InitializeOperations(OperationStatusEnum operationStatusEnum)
        {
            OperationStatusId = operationStatusEnum;
        }

        public virtual void InitializeHistory(
            string operationType, 
            string info, 
            string payload)
        {
            OperationHistory = new List<OperationHistory>()
            {
                new OperationHistory(operationType, info, payload)
            };
        }

        public virtual void AddHistory(
            string operationType,
            string info,
            string payload)
        {
            if (OperationHistory == null)
                throw new Exception("Error, history has not been initialized");

            OperationHistory.Add(new OperationHistory(operationType, info, payload));

            EntityModified();
        }

        public virtual void SetOperationStatus(OperationStatusEnum operationStatusEnum)
        {
            OperationStatusId = operationStatusEnum;

            EntityModified();
        }
    }
}
