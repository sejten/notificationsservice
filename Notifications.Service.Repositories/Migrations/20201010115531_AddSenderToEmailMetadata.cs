﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notifications.Service.Repositories.Migrations
{
    public partial class AddSenderToEmailMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sender",
                table: "EmailMetadatas");

            migrationBuilder.AlterColumn<string>(
                name: "OperationType",
                table: "OperationHistories",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(60)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OperationInfo",
                table: "OperationHistories",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(400)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "EmailRecipients",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "EmailRecipients",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EmailContent",
                table: "EmailMetadatas",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(800)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SenderId",
                table: "EmailMetadatas",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EmailSender",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SenderName = table.Column<string>(nullable: true),
                    SenderEmail = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailSender", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailMetadatas_SenderId",
                table: "EmailMetadatas",
                column: "SenderId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailMetadatas_EmailSender_SenderId",
                table: "EmailMetadatas",
                column: "SenderId",
                principalTable: "EmailSender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailMetadatas_EmailSender_SenderId",
                table: "EmailMetadatas");

            migrationBuilder.DropTable(
                name: "EmailSender");

            migrationBuilder.DropIndex(
                name: "IX_EmailMetadatas_SenderId",
                table: "EmailMetadatas");

            migrationBuilder.DropColumn(
                name: "SenderId",
                table: "EmailMetadatas");

            migrationBuilder.AlterColumn<string>(
                name: "OperationType",
                table: "OperationHistories",
                type: "nvarchar(60)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OperationInfo",
                table: "OperationHistories",
                type: "nvarchar(400)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "EmailRecipients",
                type: "nvarchar(50)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "EmailRecipients",
                type: "nvarchar(100)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EmailContent",
                table: "EmailMetadatas",
                type: "nvarchar(800)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Sender",
                table: "EmailMetadatas",
                type: "nvarchar(70)",
                nullable: true);
        }
    }
}
