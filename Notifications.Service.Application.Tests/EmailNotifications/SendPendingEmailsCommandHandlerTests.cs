﻿using Microsoft.Extensions.Options;
using Moq;
using Notifications.Service.Application.Configuration;
using Notifications.Service.Application.EmailNotifications.Commands;
using Notifications.Service.Application.EmailNotifications.Handlers;
using Notifications.Service.Application.EmailNotifications.Services;
using Notifications.Service.Domain.Email;
using Notifications.Service.Repositories.Repositories.Email;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notifications.Service.Application.Tests.EmailNotifications
{
    [TestFixture]
    public class SendPendingEmailsCommandHandlerTests
    {
        private SendPendingEmailsCommand _command;
        private SendPendingEmailsCommandHandler _sut;

        private Mock<IEmailNotificationRepository> _emailNotificationRepository;
        private Mock<IEmailNotificationValidationProvider> _emailNotificationValidationProvider;
        private Mock<ISmtpSender> _smtpSender;
        private IOptions<EmailConfiguration> _emailOptions;

        private const string _defaultSender = "test@test.pl";

        [SetUp]
        public void Setup()
        {
            _emailNotificationRepository = new Mock<IEmailNotificationRepository>();
            _emailNotificationValidationProvider = new Mock<IEmailNotificationValidationProvider>();
            _smtpSender = new Mock<ISmtpSender>();
            _command = new SendPendingEmailsCommand();

            _emailOptions = Options.Create(new  EmailConfiguration
            {
                DefaultEmailSender = _defaultSender
            });

            _sut = new SendPendingEmailsCommandHandler(
                _emailNotificationRepository.Object,
                _emailNotificationValidationProvider.Object, 
                _smtpSender.Object, 
                _emailOptions);
        }

        [Test]
        public async Task GivenSendingPendingEmailsAction_WhenNoPendingEmails_ThenShouldNotValidateAnyNotification()
        {
            //arrange
            _emailNotificationRepository.Setup(x => x.GetAllPendingEmailNotificationsAsync())
                .Returns(Task.FromResult(GetNoPendingEmails()));

            //act
            await _sut.Handle(_command, new System.Threading.CancellationToken());

            //assert
            _emailNotificationValidationProvider.Verify(x => x.Validate(It.IsAny<EmailNotification>()), Times.Never);
        }

        [Test]
        public async Task GivenSendingPendingEmailsAction_WhenPendingNotificationHasInvalidSenderOrRecipient_ShouldThrowException()
        {
            //arrange
            _emailNotificationRepository.Setup(x => x.GetAllPendingEmailNotificationsAsync())
                .Returns(Task.FromResult(GetNotificationWithWrongSender()));

            _emailNotificationValidationProvider.Setup(x => x.Validate(It.IsAny<EmailNotification>()))
                .Returns(GetValidationException());

            //assert
            Assert.ThrowsAsync<AggregateException>(async () => await _sut.Handle(_command, new System.Threading.CancellationToken()));
        }

        [Test]
        public async Task GivenSendingPendingEmailsAction_WhenPendingOperationIsValid_ShouldCallSmtpSenderAndUpdateNotificationWithDoneStatus()
        {
            //arrange
            _emailNotificationRepository.Setup(x => x.GetAllPendingEmailNotificationsAsync())
                .Returns(Task.FromResult(GetCorrectNotification()));

            _emailNotificationRepository.Setup(x => x.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(GetCorrectNotification().FirstOrDefault()));

            _emailNotificationValidationProvider.Setup(x => x.Validate(It.IsAny<EmailNotification>()))
                .Returns(GetEmptyValidationResults());

            //act
            await _sut.Handle(_command, new System.Threading.CancellationToken());

            //assert
            _smtpSender.Verify(x => x.SendAsync(It.IsAny<string>(), It.IsAny<IEnumerable<string>>(), It.IsAny<string>()), Times.Once);
            _emailNotificationRepository
                .Verify(x => x.UpdateNotificationAsync(It.IsAny<EmailNotification>(), It.IsAny<IEnumerable<Email>>()), Times.Once);

        }

        [Test]
        public async Task GivenSendingPendingEmailsAction_WhenPendingOperationIsValidBitNoSender_ShouldCallSmtpSenderWithDefaultSenderAndUpdateNotificationWithDoneStatus()
        {
            //arrange
            _emailNotificationRepository.Setup(x => x.GetAllPendingEmailNotificationsAsync())
                .Returns(Task.FromResult(GetCorrectNotificationWithoutSender()));

            _emailNotificationRepository.Setup(x => x.GetByIdAsync(It.IsAny<long>()))
                .Returns(Task.FromResult(GetCorrectNotificationWithoutSender().FirstOrDefault()));

            _emailNotificationValidationProvider.Setup(x => x.Validate(It.IsAny<EmailNotification>()))
                .Returns(GetEmptyValidationResults());

            //act
            await _sut.Handle(_command, new System.Threading.CancellationToken());

            //assert
            _smtpSender.Verify(x => x.SendAsync(_defaultSender, It.IsAny<IEnumerable<string>>(), It.IsAny<string>()), Times.Once);
            _emailNotificationRepository
                .Verify(x => x.UpdateNotificationAsync(It.IsAny<EmailNotification>(), It.IsAny<IEnumerable<Email>>()), Times.Once);

        }

        private IEnumerable<EmailNotification> GetCorrectNotificationWithoutSender()
        {
            return new List<EmailNotification>()
            {
                new EmailNotification("test", null, new List<string>() { "sender@sender"} )
            };
        }

        private IEnumerable<Exception> GetEmptyValidationResults()
        {
            return new List<Exception>();
        }

        private IEnumerable<EmailNotification> GetCorrectNotification()
        {
            return new List<EmailNotification>()
            {
                new EmailNotification("test", "sender@sender", new List<string>() { "sender@sender"} )
            };
        }

        private IEnumerable<Exception> GetValidationException()
        {
            return new List<Exception>()
            {
                new Exception("some validation exception")           
            };
        }

        private IEnumerable<EmailNotification> GetNotificationWithWrongSender()
        {
            return new List<EmailNotification>()
            {
                new EmailNotification("test", "WrongEmail", new List<string>() { "sender@sender"} )
            };
        }

        private IEnumerable<EmailNotification> GetNoPendingEmails()
        {
            return new List<EmailNotification>();
        }
    }
}
